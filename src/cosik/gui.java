package cosik;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;
import org.freixas.jcalendar.JCalendarCombo;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

import com.l2fprod.common.swing.JOutlookBar;
import com.l2fprod.common.swing.JTipOfTheDay;
import com.l2fprod.common.swing.tips.DefaultTip;
import com.l2fprod.common.swing.tips.DefaultTipModel;

/**
 * Klasa Gui jest glowna klasa. Odpowiedzialna jest ona za utworzenie oraz inicializacje frame  oraz wystartowanie aplikacji
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */

public class gui {

	private JFrame frame;
	int zatrzymanie=0;
	private JTextField Field_1;
	private JTable table;
	private JTextArea textArea;
	private JSlider nr_kolumny;
	private JSlider nr_wiersza;
	private JScrollPane scrollPane;
	/**
	 * Funkcja uruchomienia apliakcji
	 */
	public static void main(String[] args) {
	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gui window = new gui();
					window.frame.setVisible(true);
					DefaultTipModel porada = new DefaultTipModel();
				    porada.add(new DefaultTip("tip1","Zajrzyj do informacji by dowiedzieć sie więcej o autorze."));
				    porada.add(new DefaultTip("tip2","Program posiada czytelny panel nawigacyjny."));
				    porada.add(new DefaultTip("tip3","Wartości z tabeli możesz zapisać do pliku txt."));
				    porada.add(new DefaultTip("tip4","Program posiada kalendarz."));
				                    JTipOfTheDay porada_dnia = new JTipOfTheDay(porada);
				                    porada_dnia.showDialog(porada_dnia);
				                    
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Konstruktor klasy <code>Gui</code>
	 */
	public gui() {
		initialize();
	}

	/**
	 * Funkcja inicjalizujaca zawartosc gui.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 607, 489);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(143, 312, 238, 91);
	
	
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(143, 312, 238, 91);
		panel.add(scrollPane_1);

		scrollPane_1.setViewportView(textArea);
		
		
		Field_1 = new JTextField();
		Field_1.setBounds(221, 97, 110, 19);
		panel.add(Field_1);
		Field_1.setColumns(10);
		
		nr_kolumny = new JSlider();
		nr_kolumny.setFont(new Font("Tahoma", Font.PLAIN, 12));
		nr_kolumny.setPaintTicks(true);
		nr_kolumny.setPaintLabels(true);
		nr_kolumny.setMinorTickSpacing(1);
		nr_kolumny.setMajorTickSpacing(1);
		nr_kolumny.setMaximum(5);
		nr_kolumny.setMinimum(1);
		nr_kolumny.setBounds(341, 69, 179, 47);
		panel.add(nr_kolumny);
		
		nr_wiersza = new JSlider();
		nr_wiersza.setFont(new Font("Tahoma", Font.PLAIN, 15));
		nr_wiersza.setPaintLabels(true);
		nr_wiersza.setPaintTicks(true);
		nr_wiersza.setMinorTickSpacing(1);
		nr_wiersza.setMaximum(5);
		nr_wiersza.setMinimum(1);
		nr_wiersza.setMajorTickSpacing(1);
		nr_wiersza.setBounds(341, 145, 179, 51);
		panel.add(nr_wiersza);
		
		table = new JTable();
		table.setSurrendersFocusOnKeystroke(true);
		table.setEnabled(false);
		table.setColumnSelectionAllowed(true);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0},
			},
			new String[] {
				"New column", "New column", "New column", "New column", "New column"
			}
		));
		table.setBounds(127, 209, 254, 80);
		panel.add(table);
		
		JToolBar toolb = new JToolBar();
		toolb.setRollover(false);
		toolb.setFloatable( false);
		toolb.setBounds(0, 10, 499, 29);
		panel.add(toolb);
		
		JButton btnNewButton = new JButton("Dodaj");
		toolb.add(btnNewButton);
		btnNewButton.addActionListener(new Dodanie(Field_1, textArea, table, nr_kolumny, nr_wiersza) {});
		
		JButton btnNewButton_1 = new JButton("Zapisz");
		toolb.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new Zapis(textArea,Field_1, table){});
		
		JButton btnNewButton_2 = new JButton("Wyczyść");
		toolb.add(btnNewButton_2);
		btnNewButton_2.addActionListener(new Wyczysc(textArea,Field_1, table){});
		
		JButton btnNewButton_3 = new JButton("Suma");
		toolb.add(btnNewButton_3);
		btnNewButton_3.addActionListener(new Suma(textArea,Field_1, table){});
		
		JButton btnNewButton_4 = new JButton("Średnia");
		toolb.add(btnNewButton_4);
		btnNewButton_4.addActionListener(new Srednia(textArea,Field_1, table){});
		
		JButton btnNewButton_5 = new JButton("Max i Min");
		toolb.add(btnNewButton_5);
		btnNewButton_5.addActionListener(new Maximin(textArea,Field_1, table){});
		
		JButton btnNewButton_6 = new JButton("O Autorze");
		toolb.add(btnNewButton_6);
		btnNewButton_6.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
			
				Autor window = new Autor();
			}
		});
		
		JButton btnNewButton_7 = new JButton("Pomoc");
		toolb.add(btnNewButton_7);
		btnNewButton_7.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
			
				Pomoc window = new Pomoc();
			}
		});
	
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 40, 202, 24);
		panel.add(menuBar);
		
		JMenu tabela= new JMenu("Tabela");
		menuBar.add(tabela);		
						
		JMenu dzialania= new JMenu("Działania");
		menuBar.add(dzialania);
		
		JMenu pomoc= new JMenu("Pomoc");
		menuBar.add(pomoc);
		
		JMenuItem mDodanie = new JMenuItem("Dodanie");
		tabela.add(mDodanie);
		mDodanie.addActionListener(new Dodanie(Field_1, textArea, table, nr_kolumny, nr_wiersza) {});
		
		JMenuItem mZapis = new JMenuItem("Zapis");
		tabela.add(mZapis);
		mZapis.addActionListener(new Zapis(textArea,Field_1, table){});
		
		JMenuItem mWyczysc = new JMenuItem("Wyczyść");
		tabela.add(mWyczysc);
		mWyczysc.addActionListener(new Wyczysc(textArea,Field_1, table){});
		
		JMenuItem mSuma = new JMenuItem("Suma");
		dzialania.add(mSuma);
		mSuma.addActionListener(new Suma(textArea,Field_1, table){});
		
		JMenuItem mSrednia = new JMenuItem("Średnia");
		dzialania.add(mSrednia);
		mSrednia.addActionListener(new Srednia(textArea,Field_1, table){});
		
		JMenuItem mMaximin = new JMenuItem("Max i min");
		dzialania.add(mMaximin);
		mMaximin.addActionListener(new Maximin(textArea,Field_1, table){});
		
		JMenuItem mOAutorze = new JMenuItem("O Autorze");
		pomoc.add(mOAutorze);
		
		mOAutorze.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
			
				Autor window = new Autor();
			}
		});
		
		JMenuItem mPomoc = new JMenuItem("Pomoc");
		pomoc.add(mPomoc);
		
		mPomoc.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
			
				Pomoc Pomoc = new Pomoc();
			}
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		JLabel napisik = new JLabel("Wprowadz liczbe ");
		napisik.setFont(new Font("Tahoma", Font.PLAIN, 12));
		napisik.setBounds(221, 69, 101, 22);
		panel.add(napisik);
		
		
		
		JLabel tekst_kolumna = new JLabel("Numer kolumny");
		tekst_kolumna.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tekst_kolumna.setBounds(341, 37, 110, 22);
		panel.add(tekst_kolumna);
		
		JLabel Tekst_wiersz = new JLabel("Numer wiersza");
		Tekst_wiersz.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Tekst_wiersz.setBounds(341, 126, 78, 13);
		panel.add(Tekst_wiersz);
		
		
		
		JButton btnDodaj = new JButton("Dodaj");
		btnDodaj.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnDodaj.addActionListener(new Dodanie(Field_1, textArea, table, nr_kolumny, nr_wiersza) {});
		btnDodaj.setBounds(414, 206, 85, 21);
		panel.add(btnDodaj);
		
		JButton btnZapisz = new JButton("Zapisz");
		btnZapisz.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnZapisz.setBounds(414, 249, 85, 21);
		btnZapisz.addActionListener(new Zapis(textArea,Field_1, table){});
		panel.add(btnZapisz);
		
		JButton btnWyczy = new JButton("Wyczyść");
		btnWyczy.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnWyczy.setBounds(414, 291, 85, 21);
		btnWyczy.addActionListener( new  Wyczysc(textArea,  Field_1, table){}); 
		panel.add(btnWyczy);
	
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(414, 347, 141, 22);
		panel.add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Suma elementów", "Średnia Elementów", "Wartość max i min"}));
	
		int combo = 0;
		comboBox.addActionListener(new CBox(comboBox,textArea,Field_1,table){});
		
		final JCalendarCombo calendarCombo = new JCalendarCombo();
		calendarCombo.setBounds(127, 150, 164, 29);
		panel.add(calendarCombo);
		//Kalendarz 
		 final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
         calendarCombo.addDateListener(new DateListener() {
                 public void dateChanged(DateEvent arg0) {
              
							if(zatrzymanie==1)
                         {  
                                  String strDate = sdf.format(calendarCombo.getDate());
                                    
                                 String komunikat="Zmieniono datę na "+strDate+"\n";
                                 ;
                         textArea.append(komunikat);
                         ;}
                         else
                         {zatrzymanie=1;}
                 }
         });
         calendarCombo.setDateFormat(sdf);
         Date date = new Date();
         calendarCombo.setDate(date);
         
         JOutlookBar outlookBar = new JOutlookBar();
         outlookBar.setBounds(0, 97, 101, 272);
         panel.add(outlookBar);
         JPanel filePanel1 = new JPanel();
         JPanel filePanel2 = new JPanel();
         JPanel filePanel3 = new JPanel();
         outlookBar.addTab( "Tabela", null, filePanel1, null );
         outlookBar.addTab( "Działania", null, filePanel2, null );
         outlookBar.addTab( "Pomoc", null, filePanel3, null );
         
         JButton Butt1 = new JButton( "Dodaj" );
         filePanel1.add(Butt1);
         Butt1.addActionListener(new Dodanie(Field_1, textArea, table, nr_kolumny, nr_wiersza) {});
         
         JButton Butt2 = new JButton( "Zapisz" );
         filePanel1.add(Butt2);
         Butt2.addActionListener(new Zapis(textArea,Field_1, table){});
         
         JButton Butt3 = new JButton( "Wyczyść" );
         filePanel1.add(Butt3);
         Butt2.addActionListener(new Wyczysc(textArea,Field_1, table){});
         
         JButton Butt4 = new JButton( "Suma" );
         filePanel2.add(Butt4);
         Butt4.addActionListener(new Suma(textArea,Field_1, table){});
         
         JButton Butt5 = new JButton( "Średnia" );
         filePanel2.add(Butt5);
         Butt5.addActionListener(new Srednia(textArea,Field_1, table){});
         
         JButton Butt6 = new JButton( "Max i min" );
         filePanel2.add(Butt6);
         Butt6.addActionListener(new Maximin(textArea,Field_1, table){});
         
         JButton Butt7 = new JButton( "O Autorze" );
         filePanel3.add(Butt7);
         Butt7.addActionListener(new ActionListener() {
 			
 			@Override
 			public void actionPerformed(ActionEvent e) {
 				// TODO Auto-generated method stub
 			
 				Autor window = new Autor();
 			}
 		});
         
         JButton Butt8 = new JButton( "Pomoc" );
         filePanel3.add(Butt8);
         Butt8.addActionListener(new ActionListener() {
 			
 			@Override
 			public void actionPerformed(ActionEvent e) {
 				// TODO Auto-generated method stub
 			
 				Pomoc window = new Pomoc();
 			}
 		});
         
         JButton btnNewButton_8 = new JButton("Chart");
         btnNewButton_8.setBounds(414, 382, 85, 21);
         panel.add(btnNewButton_8);
         btnNewButton_8.addActionListener(new ActionListener() {
  			
  			@Override
  			public void actionPerformed(ActionEvent e) {
  			TableModel tm = table.getModel();
  			double suma;
  			String sumat;
  			double wyniczek = 0;
  			
  				for (int a=0; a<5; a++ ) {
  					for(int b=0; b<5; b++) {
  						sumat=tm.getValueAt(a,b).toString();
  						suma=Integer.parseInt(sumat);
  						wyniczek=wyniczek+suma;
  				
  					}}
  				//Utworzenie wykresu tabelki
  				DefaultPieDataset pieDataset =new DefaultPieDataset();
  		         for (int a=0; a<5; a++ ) {
  		 			for(int b=0; b<5; b++) {
  		 				
  		 					Integer liczba = (Integer) table.getValueAt(a, b);
  		 					Double innaliczba= liczba/wyniczek;
  		 					
  		 					if (innaliczba!=0 ){
  		 					pieDataset.setValue("wiersz "+(a+1)+" kolumna "+(b+1), innaliczba);
  		 			}}}
  		         
  		         
  		     
  		      
  		         JFreeChart chart= ChartFactory.createPieChart("Wykres kołowy", pieDataset,true,true,true );
  		         PiePlot P=(PiePlot)chart.getPlot();
  		         ChartFrame frame=new ChartFrame("Wykres kołowy", chart);
  		         frame.setVisible(true);
  		         frame.setSize(450,500);
  		      
  				
  				
  				
  				
  				
  				
  				
  				
  			
  				
  			}
         });
    
         
         
         
	}
}

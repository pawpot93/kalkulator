package cosik;


import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;


/**
 * Klasa Dodanie obsluguje metode dodania wartosci z JTextField do Tablicy
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */

public class Dodanie implements ActionListener {
	
//	static final Logger logger = Logger.getLogger(Window.class.getName());
	JTextArea textArea;
	JTextField Field_1;
    JTable table;
    JSlider nr_kolumny;
    JSlider nr_wiersza;

    
	/**
	 * Konstruktor klasy <code>Dodanie</code>
	 */

	public Dodanie(JTextField field_12, JTextArea textArea2, JTable table2, JSlider nr_kolumny2, JSlider nr_wiersza2) {
		
		super();
		this.Field_1 = field_12;
		this.table = table2;
		this.nr_kolumny = nr_kolumny2;
		this.nr_wiersza = nr_wiersza2;
		this.textArea = textArea2;
		
	}

	/**
	 * Funkcja dodawania wartosci do tablicy
	 */
	public void actionPerformed(ActionEvent e) {
		
		int a = 0;
		
		Integer wiersz = (Integer)nr_kolumny.getValue() -1;
        Integer kolumna = (Integer)nr_wiersza.getValue() -1;
        try{
        String liczba = Field_1.getText();
         if (Field_1.getText().equals("")){
        	 Field_1.setText("0");
            a=0;
         }
         else
         {
            a = Integer.parseInt(liczba);
            if(a>2000000)
            {
            	a=2000000;
            	Field_1.setText("2000000");}
            else if(a<-2000000)
            {
            	a=-2000000;
                Field_1.setText("-2000000");
            }
         }

         table.setValueAt(a,wiersz,kolumna);
         String informacja="Dodano wartość\n";
         textArea.append(informacja);
         
        }
         catch (Exception ioe)
         {ioe.printStackTrace();
         JOptionPane.showMessageDialog (null, "Błędna wartość wejścia\n", "Błąd", JOptionPane.INFORMATION_MESSAGE); 
         Field_1.setText("0");
        
         }
        }

	

}

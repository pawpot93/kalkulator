package cosik;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.TableModel;



/**
 * Klasa Zapis obsluguje metode zapisu wartości z tabelki do pliku
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */

public class Zapis implements ActionListener {
	JTextArea textArea;
	JTextField Field_1;
    JTable table;

    String zapis="";
	int a;
	int b;
	
	/**
	 * Konstruktor klasy <code>Zapis</code>
	 */
    public Zapis(JTextArea textArea, JTextField field_1, JTable table) {
		super();
		this.textArea = textArea;
		Field_1 = field_1;
		this.table = table;
	}
    /**
     *Funkcja powodujaca zapis tabelki do pliku
	 */
  public void actionPerformed(ActionEvent e) {

	  TableModel tm = table.getModel();
	  try {
			FileWriter fw=new FileWriter("dane.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			
				for(a = 0; a <5; a++) {
					for(b=0;b<5;b++){
					zapis=tm.getValueAt(a,b).toString();
					zapis=zapis+" ";
					bw.write(zapis);
					}
						zapis="";
						 bw.newLine();
				}
				bw.close();
		        fw.close();
				String komunikat="Zapisano\n";
				textArea.append(komunikat);
			}
			catch (IOException ee)
	        {ee.printStackTrace();
	        JOptionPane.showMessageDialog (null, "Zapisano do pliku\n", "Zapis", JOptionPane.INFORMATION_MESSAGE);
		}
    
    
  } 
}

package cosik;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;



import java.awt.Font;
import java.awt.Window;

/**
 * Klasa Pomoc obsluguje metode utworzenia okna dialogowego z informacja o programie.
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */

public class Pomoc {

	JFrame frame;



	/**
	 * Konstruktor klasy <code>Pomoc</code>
	 */
	public Pomoc() {
		initialize();
	}
	/**
	 * Funkcja utworzenia okna dialogowego
	 */
	private void initialize() {
	
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 700);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
;
		
		JDialog d = new JDialog(frame, "Pomoc"); 
		
		JTextArea l = new JTextArea("\n\nWitaj w programie realizującym funkcje matematyczne \n"
				+ "\nW okienku należy wpisać zadaną liczbę z przedziału -2000000 do 2000000, następnie wybrać wiersz i kolumnę w którym \n chcemy ją zapisać. Następnie wybrać opcję dodaj oraz opcję kalkulatora którą chcemy zrealizować."); 
		l.setEditable(false);
		
		  
          d.add(l); 
          d.setSize(700, 200); 
          d.setLocationRelativeTo(frame);
          d.setVisible(true); 
          l.setSize(d.getSize());
	}
}

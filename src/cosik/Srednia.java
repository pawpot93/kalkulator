package cosik;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.TableModel;



/**
 * Klasa Srednia obsluguje metode obliczenia sredniej wartości z tabeli 
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */

public class Srednia implements ActionListener {
	JTextArea textArea;
	JTextField Field_1;
    JTable table;
    String sumat="";
    String wiadomosc="";
	int suma=0;

	double wyniczek;
	
	/**
	 * Konstruktor klasy <code>Srednia</code>
	 */

	public Srednia(JTextArea textArea, JTextField field_1, JTable table) {
		super();
		this.textArea = textArea;
		this.Field_1 = field_1;
		this.table = table;
		
		
	}
	/**
	 * Funkcja obliczania wartości sredniej
	 */

	public void actionPerformed(ActionEvent e) {
		
		TableModel tm = table.getModel();
		
		for (int a=0; a<5; a++ ) {
			for(int b=0; b<5; b++) {
				sumat=tm.getValueAt(a,b).toString();
				suma=Integer.parseInt(sumat);
				wyniczek=wyniczek+suma;
				}}
				wyniczek=wyniczek/25;
				String wiadomosc="Średnia elementów wynosi  " +wyniczek+"\n";
				textArea.append(wiadomosc);
				wyniczek=0;
				wiadomosc="";
				sumat="";
				suma=0;
				 wyniczek=0;
				
			}
}

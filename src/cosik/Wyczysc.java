package cosik;


import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;




/**
 * Klasa Wyczysc obsluguje metode czyszczenia tabelki z wartosci wczesniej wpisanych
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */
public class Wyczysc implements ActionListener {
	

	JTextArea textArea;
	JTextField Field_1;
    JTable table;
    /**
	 * Konstruktor klasy <code>Wyczysc</code>
	 */
	public Wyczysc(JTextArea textArea, JTextField field_1, JTable table) {
		super();
		this.textArea = textArea;
		Field_1 = field_1;
		this.table = table;
	}
	
	/**
	 * Funkcja wyczyszczenia tabelki
	 */
	public void actionPerformed(ActionEvent e) {
	
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0},
				},
				new String[] {
					"New column", "New column", "New column", "New column", "New column"
				}
				));
		String komunikat = "Wyczyszczono tablicę\n "; 
		textArea.append(komunikat);
	}
	
}
package cosik;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.TableModel;



/**
 * Klasa Maximin obsluguje metode wyznaczenia wartosci maxymalnej i minimalnej tablicy.
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */


public class Maximin implements ActionListener {
	JTextArea textArea;
	JTextField Field_1;
    JTable table;
    String sumat="";
    String wiadomosc="";
	int suma=0;
	int wyniczek=0;
	int c=200000;
	int d=0;
	int max, min;

	
	/**
	 * Konstruktor klasy <code>Maximin</code>
	 */
	public Maximin(JTextArea textArea, JTextField field_1, JTable table) {
		super();
		this.textArea = textArea;
		this.Field_1 = field_1;
		this.table = table;
		
		
	}
    
	/**
	 * Funkcja wyznaczenia wartosci maksymalnej i minimalnej
	 */
	public void actionPerformed(ActionEvent e) {

		TableModel tm = table.getModel();
		
		for (int a=0; a<5; a++ ) {
			for(int b=0; b<5; b++) {
				sumat=tm.getValueAt(a,b).toString();
				suma=Integer.parseInt(sumat);
				if(suma<c) 
				{
					min=suma;
					c=suma;
				}
				else if (suma>=c)
				{
					min=c;
				}}}
		suma=0;
		for (int a=0; a<5; a++ ) {
			for(int b=0; b<5; b++) {
				sumat=tm.getValueAt(a,b).toString();
				suma=Integer.parseInt(sumat);
				if(suma>d) 
				{
					max=suma;
					d=suma;
				}
				else if (suma<=d)
				{
					max=d;
				}}}
		
		
		
				String wiadomosc="Minimum wynosi " +c+" Maximum wynosi "+d+"\n";
				textArea.append(wiadomosc);
				
				wiadomosc="";
				sumat="";
				d=0;
				c=200000;
				suma=0;
	
				
			}
}

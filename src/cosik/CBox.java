package cosik;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.TableModel;


/**
 * Klasa CBox obsluguje wszystkie zdarzenia combo boxa i implementuje ActionListener.
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */

public class CBox implements ActionListener {
	JComboBox comboBox;
	JTextArea textArea;
	JTextField Field_1;
	
    JTable table;
    String sumat="";
    String wiadomosc="";
	int suma=0;
	int combo;
	int c=200000,d=0;
	int max, min;
	int wyniczek=0;
	/**
	 * Konstruktor klasy <code>CBox</code>
	 */
	public CBox (JComboBox comboBox, JTextArea textArea, JTextField field_1, JTable table) {
		super();
		this.comboBox=comboBox;
		this.textArea = textArea;
		this.Field_1 = field_1;
		this.table = table;
		
		
	}
	/**
	 * Wszystkie funkcje tej klasy.
	 */
	public void actionPerformed(ActionEvent e) {
		combo= comboBox.getSelectedIndex();	
		if(combo==0) {
	
		TableModel tm = table.getModel();
		
		for (int a=0; a<5; a++ ) {
			for(int b=0; b<5; b++) {
				sumat=tm.getValueAt(a,b).toString();
				suma=Integer.parseInt(sumat);
				wyniczek=wyniczek+suma;
				}}
		
				String wiadomosc="Suma elementów " +wyniczek+"\n";
				textArea.append(wiadomosc);
				wyniczek=0;
				wiadomosc="";
				sumat="";
				suma=0;
				wyniczek=0;
				}
		else if(combo==1) {
			
			TableModel tm = table.getModel();
			for (int a=0; a<5; a++ ) {
				for(int b=0; b<5; b++) {
					sumat=tm.getValueAt(a,b).toString();
					suma=Integer.parseInt(sumat);
					wyniczek=wyniczek+suma;
					}}
					wyniczek=wyniczek/25;
					String wiadomosc="Średnia elementów wynosi  " +wyniczek+"\n";
					textArea.append(wiadomosc);
					wyniczek=0;
					wiadomosc="";
					sumat="";
					suma=0;
					 wyniczek=0;}
		else if(combo==2) {
			
			
			TableModel tm = table.getModel();
			
			for (int a=0; a<5; a++ ) {
				for(int b=0; b<5; b++) {
					sumat=tm.getValueAt(a,b).toString();
					suma=Integer.parseInt(sumat);
					if(suma<c) 
					{
						min=suma;
						c=suma;
					}
					else if (suma>=c)
					{
						min=c;
					}}}
			suma=0;
			for (int a=0; a<5; a++ ) {
				for(int b=0; b<5; b++) {
					sumat=tm.getValueAt(a,b).toString();
					suma=Integer.parseInt(sumat);
					if(suma>d) 
					{
						max=suma;
						d=suma;
					}
					else if (suma<=d)
					{
						max=d;
					}}}
			
			
			
					String wiadomosc="Minimum wynosi " +c+" Maximum wynosi "+d+"\n";
					textArea.append(wiadomosc);
					
					wiadomosc="";
					sumat="";
					d=0;
					c=200000;
					suma=0;
			
			
			
			
			
			
			
			
		}
}
}

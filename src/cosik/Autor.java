package cosik;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;



import java.awt.Font;
import java.awt.Window;
/**
 * Klasa Autor obsluguje metode utworzenia okna dialogowego z informacja o autorze.
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */
public class Autor {

	JFrame frame;
	

	/**
	 * Konstruktor klasy <code>Autor</code>
	 */
	public Autor() {
		initialize();
	}
	/**
	 * Funkcja utworzenia okna dialogowego
	 */
	private void initialize() {
	
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
	
		frame.getContentPane().setLayout(null);
		
;
		
		JDialog d = new JDialog(frame, "O Autorze"); 
		
		JLabel l = new JLabel("Wykonał Paweł Potoniec"); 
	
		  
          d.add(l); 
          d.setSize(200, 100); 
          d.setLocationRelativeTo(frame);
          d.setVisible(true); 
	}
}

package cosik;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.TableModel;



/**
 * Klasa Suma obsluguje metode obliczenia sumy wartosci z tabeli 
 * <p>
 * @author Pawel Potoniec
 * @since 24 April 2020
 * @version 1.0
 */

public class Suma implements ActionListener {
	
	JTextArea textArea;
	JTextField Field_1;
    JTable table;
    String sumat="";
    String wiadomosc="";
	int suma=0;
	int combo;
	int wyniczek=0;
	
	/**
	 * Konstruktor klasy <code>Suma</code>
	 */

	public Suma( JTextArea textArea, JTextField field_1, JTable table) {
		super();
		this.textArea = textArea;
		this.Field_1 = field_1;
		this.table = table;
		
		
	}
	/**
	 * Funkcja obliczania sumy wartosci z tabeli
	 */

	public void actionPerformed(ActionEvent e) {
	
		TableModel tm = table.getModel();
		
		for (int a=0; a<5; a++ ) {
			for(int b=0; b<5; b++) {
				sumat=tm.getValueAt(a,b).toString();
				suma=Integer.parseInt(sumat);
				wyniczek=wyniczek+suma;
				}}
		
				String wiadomosc="Suma elementów " +wyniczek+"\n";
				textArea.append(wiadomosc);
				wyniczek=0;
				wiadomosc="";
				sumat="";
				suma=0;
				wyniczek=0;
				
			}
}
